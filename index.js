console.log('Hello, World')
// Details
const details = {
    fName: "Kevin",
    lName: "Candido",
    age: 18,
    hobbies : [
        "watching movies", "playing video games", "going out"
    ] ,
    workAddress: {
        housenumber: "Number 7",
        street: "Ninong Jovito",
        city: "Caloocan City",
        state: "Philippines",
    }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.fName)
console.log("My Last Name is " + details.lName)
console.log(`Yes, I am ${details.fName} ${details.lName}.`)
console.log("I am " + details.age + " years old.")
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");